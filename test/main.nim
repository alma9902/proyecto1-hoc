import os
import strutils
include nuevo

const
  Delim = '\t'

#[PRIMER PARÁMETRO = LA INSTANCIA CON LA CUAL SE VA A TRABAJAR
                    1 = instancia de 40 ciudades
                    2 = intancia de 150 ciudades
  SEGUNDO PARÁMETRO = LA SEMILLA
]#
proc main()=
  var instance = parseInt(paramStr(1))
  var seed = parseInt(paramStr(2))
  if seed == 0:
    echo "Intenta de nuevo con una semilla mayor que 0"
  if instance == 1:
    executeWithInstance1(seed)
  elif instance == 2:
    executeWithInstance2(seed)
  elif instance == 1040:
    getSeeds40()
  elif instance == 20150:
    getSeeds150()
  else:
    echo "Intenta de nuevo :( "

main()
