include ./../model/Solution
#include ./../model/Database
include ./../model/SimulatedAnnealing

proc executeWithInstance1(seed: int)=
  var seeds:seq[int]
  var db = Database(
    name:"TSP"
  )
  var instance = loadInstance("instance1.txt")
  var cities = db.getCitiesByInstance(instance)
  var indices: seq[int]
  for i in 0..<len(instance):
    indices.add(i)
  var (nodes,maximum) = db.getMatrixAndMaximum(indices,cities)
  var graph: Graph[40]
  new(graph)
  graph.indices = indices
  var connectedCities = graph.setInitialMatrix(nodes,maximum)
  graph.setNormalizador(connectedCities)
  var costo = graph.calculaCosto()
  graph.setCosto(costo)
  var respaldo = graph
  echo "EJECUTANDO RECOCIDO SIMULADO PARA LA INSTANCIA DE 40"
  echo "LA SEMILLA ES ", seed
  setSeed(seed)
  var t:float = 8.0
  var porcentaje:float = 0.95
  var tempInit = graph.temperaturaInicial(t, porcentaje)
  graph.aceptacionPorUmbrales(tempInit)
  echo "EL COSTO FINAL ES = ",graph.costo
  graph.printInstance(cities,seed)

proc executeWithInstance2(seed: int)=
  var seeds:seq[int]
  var db = Database(
    name:"TSP"
  )
  var instance = loadInstance("instance2.txt")
  var cities = db.getCitiesByInstance(instance)
  var indices: seq[int]
  for i in 0..<len(instance):
    indices.add(i)
  var (nodes,maximum) = db.getMatrixAndMaximum(indices,cities)
  var graph: Graph[150]
  new(graph)
  graph.indices = indices
  var connectedCities = graph.setInitialMatrix(nodes,maximum)
  graph.setNormalizador(connectedCities)
  var costo = graph.calculaCosto()
  graph.setCosto(costo)
  var respaldo = graph
  echo "EJECUTANDO RECOCIDO SIMULADO PARA LA INSTANCIA DE 150"
  echo "LA SEMILLA ES ", seed
  setSeed(seed)
  var t:float = 8.0
  var porcentaje:float = 0.95
  var tempInit = graph.temperaturaInicial(t, porcentaje)
  graph.aceptacionPorUmbrales(tempInit)
  echo "EL COSTO FINAL ES = ",graph.costo
  graph.printInstance(cities,seed)

proc getSeeds40()=
  var seeds:seq[int]
  var db = Database(
    name:"TSP"
  )
  var instance = loadInstance("instance1.txt")
  var cities = db.getCitiesByInstance(instance)
  var indices: seq[int]
  for i in 0..<len(instance):
    indices.add(i)
  var (nodes,maximum) = db.getMatrixAndMaximum(indices,cities)
  var graph: Graph[40]
  new(graph)
  graph.indices = indices
  var connectedCities = graph.setInitialMatrix(nodes,maximum)
  graph.setNormalizador(connectedCities)
  var costo = graph.calculaCosto()
  graph.setCosto(costo)
  var respaldo = graph
  echo "EJECUTANDO RECOCIDO SIMULADO PARA LA INSTANCIA DE 40"
  #cuántas semillas vamos a probar
  for i in 1..<300:
    echo "PROBANDO CON SEMILLA = ",i
    setSeed(i)
    var t:float = 8.0
    var porcentaje:float = 0.95
    var tempInit = graph.temperaturaInicial(t, porcentaje)
    graph.aceptacionPorUmbrales(tempInit)
    if graph.costo <= 0.24:
      seeds.add(i)
      graph.printInstance(cities,i)
    graph = respaldo

  printSeeds40(seeds)

proc getSeeds150()=
  var seeds:seq[int]
  var db = Database(
    name:"TSP"
  )
  var instance = loadInstance("instance2.txt")
  var cities = db.getCitiesByInstance(instance)
  var indices: seq[int]
  for i in 0..<len(instance):
    indices.add(i)
  var (nodes,maximum) = db.getMatrixAndMaximum(indices,cities)
  var graph: Graph[150]
  new(graph)
  graph.indices = indices
  var connectedCities = graph.setInitialMatrix(nodes,maximum)
  graph.setNormalizador(connectedCities)
  var costo = graph.calculaCosto()
  graph.setCosto(costo)
  var respaldo = graph
  echo "EJECUTANDO RECOCIDO SIMULADO PARA LA INSTANCIA 150"
  #cuántas semillas vamos a probar
  for i in 1..<200:
    echo "PROBANDO CON SEMILLA = ",i
    setSeed(i)
    var t:float = 8.0
    var porcentaje:float = 0.95
    var tempInit = graph.temperaturaInicial(t, porcentaje)
    graph.aceptacionPorUmbrales(tempInit)
    if graph.costo <= 0.24:
      seeds.add(i)
      graph.printInstance(cities,i)
    graph = respaldo

  printSeeds150(seeds)

#getSeeds40()
#getSeeds150()
