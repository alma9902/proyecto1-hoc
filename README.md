Dentro de la carpeta test, ejecutar los siguientes comandos

nim c -r --verbosity:0 --hints:off main.nim instance seed

donde instance es la instancia de la cual se quiere obtener una solución y seed la semilla con la se desea probar. Entonces, si se desea obtener la solución de la instancia de cuarenta ciudades, ejecutar:
nim c -r --verbosity:0 --hints:off main.nim 1 seed

si se desea obtener la solución de 150, ejecutar:
nim c -r --verbosity:0 --hints:off main.nim 2 seed

Si se desea correr las 300 semillas para la instancia de 40:
nim c -r --verbosity:0 --hints:off main.nim 1040 1

Para la de 150:
nim c -r --verbosity:0 --hints:off main.nim 20150 1

El reporte se encuentra dentro de la carpeta Reporte y ahí está la especificación de las mejores semillas obtenidas.
