import db_sqlite
import tables
include City
import strutils

type
  Database* = object
    name* : string

proc getCities*(database:  Database): Table[int, City] =
  let db = open("./../resources/"&database.name&".db", "", "", "")
  var i: int = 0
  var
    cities = initTable[int, City]()
  for x in db.fastRows(sql"SELECT * FROM cities"):
    var city = City(
      id: parseInt(x[0]),
      name: x[1],
      country: x[2],
      population:parseInt(x[3]),
      latitude: parseFloat(x[4]),
      longitude: parseFloat(x[5])
    )
    cities[i] = city
    i += 1
  db.close()
  return cities

proc getCitiesByInstance*(database: Database, instance: openArray[int]): Table[int, City]=
  let db = open("./../resources/"&database.name&".db", "","","")
  var
    cities = initTable[int, City]()
  for i in low(instance)..high(instance):
    for x in db.fastRows(sql"SELECT * FROM cities WHERE id = ?", instance[i]):
      var city = City(
        id: parseInt(x[0]),
        name: x[1],
        country: x[2],
        population:parseInt(x[3]),
        latitude: parseFloat(x[4]),
        longitude: parseFloat(x[5])
      )
      cities[i] = city
  db.close()
  return cities

proc checkCities*(database:Database, idCity1,idCity2:int):bool =
  let db = open("./../resources/"&database.name&".db", "", "", "")
  var connected:bool
  connected = false
  var value = db.getValue(sql"SELECT distance FROM connections WHERE id_city_1 = ? and id_city_2 = ?", idCity1,idCity2)
  #var value2 = db.getValue(sql"SELECT distance FROM connections WHERE id_city_1 = ? and id_city_2 = ?", idCity2,idCity1)
  db.close()
  return value != ""

proc getDistance*(database:Database,city1,city2:City):float =
  let db = open("./../resources/"&database.name&".db", "","","")
  var value = db.getValue(sql"SELECT distance FROM connections WHERE id_city_1 = ? and id_city_2 = ?", city1.id,city2.id)
  var value2 = db.getValue(sql"SELECT distance FROM connections WHERE id_city_1 = ? and id_city_2 = ?", city2.id,city1.id)
  db.close()
  if value != "":
    return parseFloat(value)
  else:
    return parseFloat(value2)
