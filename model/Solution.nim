include Database
import strutils
import algorithm
import random
#import strutils
type
  Node* = object
    connected:bool
    distance:float

type
  Graph*[dim:static[int]] = ref object
    indices*: seq[int]
    matrix* : array[0..dim-1,array[0..dim-1,float]]
    costo*:float
    norm*:float
    #definimos el tipo matriz para representar la gráfica de la instancia

proc loadInstance*(fileName: string): seq[int]=
  let f = open("./../resources/"&filename)
  var instance : seq[int]
  var word: string
  for line in f.lines:
    for letter in line:
      if letter != ',':
        word = word & letter
      else:
        instance.add(parseInt(word))
        word= ""
    if word != "":
      instance.add(parseInt(word))
  return instance

proc setInitialMatrix(graph: var Graph, nodes:seq[seq[Node]], maxDistance:float):seq[float] =
  var cols: int = 0
  var aux : int = 0
  var listWeights: seq[float]
  for i in low(nodes)..high(nodes):
    for j in low(nodes[i])..high(nodes[i]):
      var index = j+aux
      var distance = nodes[i][j].distance
      if not nodes[i][j].connected:
        distance = distance * maxDistance
      else:
        listWeights.add(distance)
      graph.matrix[i][index] = distance
      graph.matrix[index][i] = distance
    aux += 1

  return listWeights


proc getMatrixAndMaximum(database: Database, instance : openArray[int],cities: Table[int,City]):(seq[seq[Node]],float) =
  var maxDistance: float = 0
  var tam = len(instance)
  var numCols: int = 0
  var matrix: seq[seq[Node]]
  var aux: int = 0

  for i in 0..<tam:
    var cols: seq[Node]
    for j in numCols..<tam:
      var city1 = cities[instance[i]]
      var city2 = cities[instance[j]]
      var connected: bool
      var distance: float
      var node: Node
      if i == j:
        connected = false
      else:
        connected = database.checkCities(city1.id,city2.id)

      if connected:
        aux += 1
        distance = database.getDistance(city1,city2)
        #actualizamos el máximo si es necesario
        if distance > maxDistance:
          maxDistance = distance
      else:
        distance = getNaturalDistance(city1,city2)
      node = Node(
        connected: connected,
        distance : distance
      )
      cols.add(node)
    numCols += 1
    matrix.add(cols)

  return (matrix,maxDistance)


proc getSumConsecutiveCities(graph:var Graph):float =
  var suma:float = 0
  var long:int = len(graph.indices)
  for i in 0..<long:
    if i+1 == long:
      break
    suma += graph.matrix[graph.indices[i]][graph.indices[i+1]]
  return suma

proc setNormalizador(graph: var Graph, listWeights: var seq[float]) =
  var sum: float = 0
  sort(listWeights,system.cmp[float],Descending)
  for i in low(graph.indices)..high(graph.indices)-1:
    sum += listWeights[i]

  graph.norm = sum

proc calculaCosto(graph:var Graph):float =

  var weights:float = graph.getSumConsecutiveCities()
  var costo:float

  costo = weights/graph.norm

  return costo

proc setCosto(graph: var Graph, costo : float) =
  graph.costo = costo

proc setSeed(seed: int)=
  randomize(seed)

proc getVecino(graph: var Graph)=
  var rango = len(graph.indices)-1
  var indice1,indice2, aux1, aux2: int

  while(indice1 == indice2):
    indice1 = rand(rango)
    indice2 = rand(rango)

  aux1 = graph.indices[indice1]
  aux2 = graph.indices[indice2]
  graph.indices[indice1] = aux2
  graph.indices[indice2] = aux1

  #graph.updateMatrix(indice1,indice2)

proc printInstance(graph: var Graph,cities: Table[int,City],seed:int)=
  var long:int = len(graph.indices)
  var resultado:string
  var name:string
  var costo:int
  costo = int(graph.costo * 1000)
  if len(graph.indices) == 40:
    name = "./../solutions40/"
  else:
    name = "./../solutions150/"
  name = name & "cost:"&intToStr(costo)&".txt"

  resultado = "SEMILLA: "&intToStr(seed)&"\n"

  for i in 0..<long:
    var id = cities[graph.indices[i]].id
    var r = intToStr(id)
    if i != long - 1:
      r = r & ", "
    resultado = resultado & r
  writeFile(name,resultado)
  echo "INSTANCIA FINAL"
  echo resultado
  echo "RESPALDADA EN "&name

proc printSeeds40(seeds:seq[int])=
  var name:string = "./../solutions40/SEEDS.txt"
  var resultado:string
  var long:int = len(seeds)
  for i in 0..<long:
    var seed= seeds[i]
    var r = intToStr(seed)
    if i != long-1:
      r = r & ", "
    resultado = resultado & r
  writeFile(name,resultado)

proc printSeeds150(seeds:seq[int])=
  var name:string = "./../solutions150/SEEDS.txt"
  var resultado:string
  var long:int = len(seeds)
  for i in 0..<long:
    var seed= seeds[i]
    var r = intToStr(seed)
    if i != long-1:
      r = r & ", "
    resultado = resultado & r
  writeFile(name,resultado)
