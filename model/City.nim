import math
type
  City* = object
    id*: int
    name*: string
    country*: string
    population*: int
    latitude*: float
    longitude*:  float

proc rad(grados: float):float=
  return grados*PI / 180

proc getNaturalDistance*(city1,city2: City): float=
  var A:float
  var C:float
  let earthRatio: float = 6373000
  let latV:float = rad(city1.latitude)
  let latU:float = rad(city2.latitude)
  let lonV:float = rad(city1.longitude)
  let lonU:float = rad(city2.longitude)

  var difLatitudes =latV-latU
  var difLongitudes =lonV-lonU

  A = pow(sin(difLatitudes/2),2)+(cos(latU)*cos(latV)*pow(sin(difLongitudes/2),2))
  C = 2 * arctan2(sqrt(A),sqrt(1-A))
  return earthRatio*C
