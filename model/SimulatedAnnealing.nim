import Solution
import math
import algorithm

proc calculaLote(graph: var Graph, T:float):float =
  var c: int = 0
  var r: float = 0
  var intentos: int = 0
  var L:int = int(len(graph.indices)^2 / 2)
  var costoVecino: float
  while (c < L):
    if intentos == L * 10:
      break

    var indicesActuales = graph.indices
    graph.getVecino()
    costoVecino = graph.calculaCosto()

    if costoVecino <= graph.costo+T:
      graph.setCosto(costoVecino)
      c += 1
      r += costoVecino
    else:
      graph.indices = indicesActuales
    intentos += 1

  return r / float(L)

proc aceptacionPorUmbrales(graph: var Graph, T: var float) =
  var p, q :float = 0
  var epsilon:float = 0.00001
  var phi: float = 0.95
  var lotesIguales:int = 0
  var loteAnterior:float

  while (T > epsilon):
    if lotesIguales == 20:
      break
    q = high(BiggestFloat)
    #echo "TEMPERATURA = ",T
    while (p <= q):
      if lotesIguales == 20:
        break
      q = p
      p = graph.calculaLote(T)
      if loteAnterior == graph.costo:
        lotesIguales += 1
      loteAnterior = graph.costo
      #echo "EL COSTO PARA ESTE LOTE ES : ",graph.costo
      #echo "LOS LOTES IGUALES AQUÍ SON ",lotesIguales


    T = phi*T


proc porcentajeAceptados(graph: var Graph, T: float):float =
  var c, N : float
  var costoVecino: float
  c = 0
  N = len(graph.indices)^2 / 4
  var respaldoIndices = graph.indices
  var respaldoCosto = graph.costo

  for i in 0..<int(N):
    var indicesActuales = graph.indices
    graph.getVecino()
    costoVecino = graph.calculaCosto()

    if costoVecino <= graph.costo + T:
      graph.setCosto(costoVecino)
      c += 1
    else:
      graph.indices = indicesActuales

  #para que en realidad no modifique la gráfica
  graph.indices = respaldoIndices
  graph.costo = respaldoCosto

  return c/N

proc busquedaBinaria(graph: var Graph, T1,T2,P: var float):float =
  var Tm,p : float
  var epsilon:float = 0.001

  Tm = (T1+T2) / 2
  if T2-T1 < epsilon:
    return Tm
  p = graph.porcentajeAceptados(Tm)
  if abs(P-p) <= epsilon:
    return Tm
  if p > P:
    return graph.busquedaBinaria(T1,Tm,P)
  else:
    return graph.busquedaBinaria(Tm,T2,P)


proc temperaturaInicial(graph: var Graph, T,P: var float):float =
  var p,epsilon,T1,T2,TF:float
  epsilon = 0.001
  p = graph.porcentajeAceptados(T)

  if abs(P-p) <= epsilon:
    return T
  if p < P:
    while (p < P):
      T = T*2
      p = graph.porcentajeAceptados(T)
    T1 = T/2
    T2 = T
  else:
    while (p > P):
      T = T/2
      p = graph.porcentajeAceptados(T)
    T1 = T
    T2 = T * 2

  TF= graph.busquedaBinaria(T1,T2,P)
  return TF

proc getBestGraphOfBackUp(graphs: var openArray[Graph]):Graph =
  var costos,aux: seq[float]
  var indice: int
  for i in low(graphs)..high(graphs):
    costos.add(graphs[i].costo)

  aux = sorted(costos,system.cmp[float])
  indice = binarySearch(costos,aux[0])

  return graphs[indice]
